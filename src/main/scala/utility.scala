import java.io.{BufferedInputStream, BufferedOutputStream, BufferedReader, DataInputStream, DataOutputStream, File, FileInputStream, FileOutputStream, IOException, InputStreamReader}
import java.net.URL
import sys.process._
import java.io.File
import java.nio.file.{Files, Path, Paths}

trait utility {
  case class ExecResult(result: Int, out: List[String], err: List[String])

  def exec(cmd: Seq[String]): ExecResult = {
    import scala.collection.mutable._
    import scala.sys.process._

    val out = ArrayBuffer[String]()
    val err = ArrayBuffer[String]()

    val logger = ProcessLogger(
      (o: String) => out += o,
      (e: String) => err += e
    )

    val r = Process(cmd) ! logger

    ExecResult(r, out.toList, err.toList)
  }

  def deleteDirectory(directory: File): Unit = {
    if(directory.isDirectory){
      for(file <- directory.listFiles)
        deleteDirectory(file)
    }
    directory.delete()
  }

  def resetTempDirectory: Unit = {
    deleteDirectory(new File("temp"))
    Files.createDirectory(Paths.get("temp"))
  }

  def copyResource(paths: String*): Unit = {
    for (path <- paths){
      val is = getClass.getResourceAsStream(path)
      val dataOutStream = new DataOutputStream(
        new BufferedOutputStream(
          new FileOutputStream(Paths.get("temp",path).toString)
        )
      )
      try{
        val b = new Array[Byte](4096)
        var readByte = is.read(b)
        while (-1 != readByte) {
          dataOutStream.write(b, 0, readByte)
          readByte = is.read(b)
        }
      } finally {
        is.close()
        dataOutStream.close()
      }
    }
  }

  import java.io.FileInputStream
  import java.io.FileOutputStream
  import java.io.IOException
  import java.io.InputStream
  import java.io.OutputStream

  @throws[IOException]
  def copyFolder(src: File, dest: File): Unit = {
    if (src.isDirectory) { //if directory not exists, create it
      if (!dest.exists) {
        dest.mkdir
        System.out.println("Directory copied from " + src + "  to " + dest)
      }
      //list all the directory contents
      val files = src.list
      for (file <- files) { //construct the src and dest file structure
        val srcFile = new File(src, file)
        val destFile = new File(dest, file)
        //recursive copy
        copyFolder(srcFile, destFile)
      }
    }
    else { //if file, then copy it
      //Use bytes stream to support all file types
      val in = new FileInputStream(src)
      val out = new FileOutputStream(dest)
      val buffer = new Array[Byte](1024)
      var length = in.read(buffer)
      //copy the file content in bytes
      while (0 < length) {
        out.write(buffer, 0, length)
        length = in.read(buffer)
      }
      in.close()
      out.close()
      System.out.println("File copied from " + src + " to " + dest)
    }
  }

  def downloadFile(url: String, fileName: String): Unit = {
    import scala.language.postfixOps
    try {
      new URL(url) #> new File(fileName) !!
    } catch {
      case _: java.lang.RuntimeException => {
        println("downloadFile retrying...")
        downloadFile(url, fileName)
      }
    }
  }

  def unZip(zipFile: String)={
    val result = exec(Seq("powershell","-c","Expand-Archive","-Path",zipFile,"-DestinationPath",Paths.get("temp","modpack").toString))
  }
}
