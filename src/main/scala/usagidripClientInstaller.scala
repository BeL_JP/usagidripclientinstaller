import java.awt.event.{ActionEvent, ActionListener, WindowAdapter}
import java.io.{BufferedReader, IOException, InputStream, InputStreamReader}
import java.nio.file.{Files, Paths}

import javax.swing.{JButton, JDialog, JLabel}

object usagidripClientInstaller extends App with softEtherVPNInstaller with modPackInstaller {
  val dialog = new JDialog()
  dialog.setSize(300,200)
  dialog.setTitle("usagidrip Client Installer")
  val btnOk = new JButton("Starting...")
  btnOk.setEnabled(false)
  btnOk.addActionListener(new ActionListener {
    override def actionPerformed(e: ActionEvent): Unit = {
      sys.exit
    }
  })
  dialog.getContentPane.add(btnOk)
  dialog.setAlwaysOnTop(true)
  dialog.setVisible(true)
  dialog.addWindowListener(new WindowAdapter {

  })
  btnOk.setText("Reset temp directory")
  println("resetTempDirectory Start")
  resetTempDirectory
  println("resetTempDirectory End")

  btnOk.setText("Copy resource")
  println("copyResource Start")
  copyResource("usagidrip.vpn","README.txt")
  println("copyResource End")

  btnOk.setText("Download SE-VPN")
  println("downloadSEVPN Start")
  downLoadSEVPN
  println("downloadSEVPN End")

  btnOk.setText("Install SE-VPN")
  println("installSEVPN Start")
  installSEVPN
  println("installSEVPN End")

  btnOk.setText("Install VPN profile")
  println("installVPNProfile Start")
  installVPNProfile
  println("installVPNProfile End")

  btnOk.setText("Connect VPN")
  println("connectVPN Start")
  connectVPN
  println("connectVPN End")

  btnOk.setText("Download modpack")
  println("downloadModPack Start")
  downloadModPack
  println("downloadModPack End")

  btnOk.setText("Disconnect VPN")
  println("disconnectVPN Start")
  disconnectVPN
  println("disconnectVPN End")

  btnOk.setText("Install modpack")
  println("installModPack Start")
  installModPack
  println("installModPack End")

  btnOk.setEnabled(true)
  btnOk.setText("Done!")
}
