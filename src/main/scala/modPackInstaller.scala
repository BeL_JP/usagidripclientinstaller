import java.io.{File, FileInputStream, FileOutputStream, OutputStreamWriter, PrintWriter}
import java.net.ConnectException
import java.nio.file.{Files, Paths}

import play.api.libs.json._

trait modPackInstaller extends utility {
  val modPackUrl = "http://10.243.0.1:25600/modpack.zip"
  def downloadModPack: Unit = {
    Files.deleteIfExists(Paths.get("temp", "modpack.zip"))
      downloadFile(modPackUrl, Paths.get("temp", "modpack.zip").toString)
  }

  def installModPack  = {
    unZip(Paths.get("temp","modpack.zip").toString)
    val gamedir = Paths.get("temp","modpack","usagidrip_gamedir").toString
    val forge = Paths.get("temp","modpack","versions","1.12.2-forge-14.23.5.2854").toString
    val gamedirdest = Paths.get(getMinecraftDirectory,"usagidrip_gamedir").toString
    val forgedest = Paths.get(getMinecraftDirectory,"versions","1.12.2-forge-14.23.5.2854").toString
    copyFolder(new File(gamedir),new File(gamedirdest))
    copyFolder(new File(forge),new File(forgedest))
    installMinecraftLauncherProfile
  }

  def getMinecraftDirectory: String = {
    Paths.get(System.getProperty("user.home"),"AppData","Roaming",".minecraft").toString
  }

  def installMinecraftLauncherProfile = {
    val jsondir = Paths.get(getMinecraftDirectory, "launcher_profiles.json").toString
    val templateJsonStr =
      """
        |{
        | "created" : "2020-05-22T12:09:42.424Z",
        | "gameDir" : "%GAMEDIR%",
        | "icon" : "Cake",
        | "javaArgs" : "-Xmx8G -Xms8G -XX:+UseParallelGC -XX:ParallelGCThreads=3 -XX:MaxGCPauseMillis=3 -Xmn256M -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.ignorePatchDiscrepancies=true",
        | "lastUsed" : "2020-05-22T12:59:00.015Z",
        | "lastVersionId" : "1.12.2-forge-14.23.5.2854",
        | "name" : "usagidrip",
        | "type" : "custom"
        |}
        |"""
        .stripMargin
        .replace("\r\n","")
        .replace("%GAMEDIR%", Paths.get(getMinecraftDirectory,"usagidrip_gamedir").toString.replace("\\","\\\\"))
    val templateJson = Json.parse(templateJsonStr)
    val newJson: JsValue = Files.exists(Paths.get(jsondir)) match {
      case true =>
        val inputJson = {
          val is = new FileInputStream(jsondir)
          try Json.parse(is)
          finally {
            is.close
            Files.delete(Paths.get(jsondir))
          }
        }
        val profile = (inputJson \ "profiles").get.as[JsObject]
        val newProfile = profile.+("usagidrip" , templateJson)
        JsObject(
          Seq(
            "authenticationDatabase" -> (inputJson \ "authenticationDatabase").get.as[JsObject],
            "clientToken" -> (inputJson \ "clientToken").get.as[JsString],
            "launcherVersion" -> (inputJson \ "launcherVersion").get.as[JsObject],
            "selectedUser" -> (inputJson \ "selectedUser").get.as[JsObject],
            "settings" -> (inputJson \ "settings").get.as[JsObject],
            "profiles" -> newProfile,
          )
        )
      case false =>
        JsObject(
          Seq(
            "profiles"->JsObject(
              Seq(
              "usagidrip" -> templateJson.as[JsObject]
              )
            )
          )
        )
    }

    val os = new FileOutputStream(jsondir)
    val osw = new OutputStreamWriter(os,"UTF-8")
    val pw = new PrintWriter(osw)
    try pw.print(Json.stringify(newJson)) finally pw.close
  }
}
