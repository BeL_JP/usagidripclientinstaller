trait softEtherVPNInstaller extends utility {
  val seVPNUrl = "http://www.softether-download.com/files/softether/v4.29-9680-rtm-2019.02.28-tree/Windows/SoftEther_VPN_Client/softether-vpnclient-v4.29-9680-rtm-2019.02.28-windows-x86_x64-intel.exe"
  def downLoadSEVPN = {
    downloadFile(seVPNUrl,"temp/sevpnc.exe")
  }

  def installSEVPN = {
    val result = exec(Seq("temp\\sevpnc.exe"))
    if(result.result != 0)
      sys.exit(1)
  }

  def installVPNProfile = {
    val result = exec(Seq("vpncmd","localhost","/CLIENT","/CMD","AccountImport",System.getProperty("user.dir")+"\\temp\\usagidrip.vpn"))
  }

  def connectVPN = {
    val result = exec(Seq("vpncmd","localhost","/CLIENT","/CMD","AccountConnect","usagidrip_vpn"))
  }

  def disconnectVPN = {
    val result = exec(Seq("vpncmd","localhost","/CLIENT","/CMD","AccountDisconnect","usagidrip_vpn"))
  }
}
