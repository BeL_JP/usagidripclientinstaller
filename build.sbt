name := "usagidripClientInstaller"

version := "0.2"

scalaVersion := "2.13.2"
libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.8.1"
)

assemblyJarName := s"${name.value}-${version.value}.jar"

assemblyMergeStrategy in assembly := {
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}